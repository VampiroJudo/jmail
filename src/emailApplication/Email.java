package emailApplication;

import java.util.Scanner;

public class Email {

    private String firstName;
    private String lastName;
    private String password;
    private int defaultPasswordLength = 10;
    private String department;
    private String companyName = "vampirotech.com";
    private String email;
    private int mailBoxCapacity = 500;
    private String  alternateEmail;

    public Email(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
        System.out.println(this.firstName + " " + this.lastName);

        this.department = setDepartment();
        System.out.println("Department: " + this.department + "\n");

        email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + department.toLowerCase() + "." + companyName;
        System.out.println("Your new email address is: " + this.email);

        this.password = setPassword(defaultPasswordLength);
        System.out.println("New Password is: " + this.password);
    }

    private String setDepartment() {
        System.out.print("Enter Department:\n1. Sales\n2. Accounting\n3. IT\n4. Marketing\n5. Human Resources\n\nEnter Department Code: ");
        Scanner in = new Scanner(System.in);
        int deptChosen = in.nextInt();
        switch(deptChosen) {
            case 1 :
                return "Sales";
            case 2 :
                return "Accounting";
            case 3 :
                return "IT";
            case 4 :
                return "Marketing";
            case 5 :
                return "Human Resources";
            default :
                return "None";
        }
    }

    private String setPassword(int length) {
        String passwordChar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
        char[] password = new char[length];
        for(int i = 0; i < length; i++) {
            int randNum = (int) (Math.random() * passwordChar.length());
            password[i] = passwordChar.charAt(randNum);
        }
        return new String(password);
    }

    public void setMailBoxCapacity(int capacity) {
        this.mailBoxCapacity = capacity;
    }

    public void setAlternateEmail(String altEmail) {
        this.alternateEmail = altEmail;
    }

    public void changePassword(String password) {
        this.password = password;
    }

    public int getMailBoxCapacity() {
        return mailBoxCapacity;
    }

    public String getAltEmail() {
        return alternateEmail;
    }

    public String getPassword() {
        return password;
    }

    public String showInfo() {
        return "New employee name: " + firstName + " " + lastName + "\n"+
                "Company EMail: " + email + "\n" +
                "Mailbox Capacity: " + mailBoxCapacity + "mb";
    }
}
